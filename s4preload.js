$(function(){
///////////////////////////////////////////////

    /* IMAGE SETTINGS */
    let imagePath = 'loading.gif';
    let imageWidth = 50;

    /* FONT SETTINGS */
    let fontColor = 'inherit'; // "Inherit" om CSS van parent over te nemen
    let fontSize  = "1.2em";   // "Inherit" om CSS van parent over te nemen

///////////////////////////////////////////////

    $(".s4preload").click(function(event){
        let eventEl = $(event.target);
        let clEventEl = eventEl.data("preload-text");
        if(clEventEl === undefined){clEventEl = "";}
        let overlay = $(`<div id='loaderOverlay'>
        <div class='loaderOverlayContent noselect'>
        <img width='${imageWidth}' src='${imagePath}'>
        <span style='color:${fontColor};
        font-size:${fontSize}'>${clEventEl}
        </span></div></div>`);
        $("body").append(overlay);
        $("#loaderOverlay").fadeIn();
      });
})

////////////////////////////////////////////////
